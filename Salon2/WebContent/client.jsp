<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Salon</title>
</head>

<body>
	<sql:setDataSource var="dataBase" driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/salon" user="root"
		password="4815162342" />

	<sql:query dataSource="${dataBase}" var="masters">
		SELECT * FROM masters
	</sql:query>

	<sql:query dataSource="${dataBase}" var="services">
		SELECT name FROM services
	</sql:query>

	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<div>
		<form action="AddOrder" method="post">
			<div>
				<label for="service">Choose service:</label> <select id="service"
					name="service">
					<c:forEach var="service" items="${services.rows}">
						<option>
							<c:out value="${service.name}" />
						</option>
					</c:forEach>
				</select>
			</div>

			<div>
				<label for="master">Choose master:</label> <select id="master"
					name="master">
					<c:forEach var="master" items="${masters.rows}">
						<sql:query dataSource="${dataBase}" var="ratings">
							SELECT AVG(rate) AS rate
							FROM masters_ratings
							WHERE masterID = (
							SELECT id FROM masters
							WHERE email = '<c:out value="${master.email}" />')
						</sql:query>

						<option>
							<c:out value="${master.name}" />
							<c:out value="${master.secondName}" />
							<c:forEach var="rating" items="${ratings.rows}">
								<c:choose>
									<c:when test="${rating.rate != null}">
										<c:out value="(${rating.rate})" />
									</c:when>
									<c:otherwise>
										<c:out value="(not rated)" />
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</option>
					</c:forEach>
				</select>
			</div>

			<div>
				<label for="timeslot">Choose time:</label> <select id="timeslot"
					name="timeslot">
					<c:forEach begin="0" end="9" varStatus="loop">
						<option><c:out value="${8 + loop.index}:00" /></option>
					</c:forEach>
				</select>
			</div>

			<button type="submit">Make order</button>
		</form>
	</div>
</body>
</html>