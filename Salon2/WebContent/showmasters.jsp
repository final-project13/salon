<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Salon</title>
</head>

<body>
	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<div>
		<form action="AddMaster" method="post">
			<div>
				<label for="name">Name</label> <input type="text" name="name"
					id="name" placeholder="Name" />
			</div>
			<div>
				<label for="secondname">Second name</label> <input type="text"
					name="secondname" id="secondname" placeholder="Second name" />
			</div>
			<div>
				<label for="email">Email</label> <input type="email" name="email"
					id="email" placeholder="Email" />
			</div>
			<div>
				<label for="password">Password</label> <input type="password"
					name="password" id="password" placeholder="Password" />
			</div>
			<div>
				<label for="confpass">Confirm password</label> <input
					type="password" name="confpass" id="confpass"
					placeholder="Password" />
			</div>
			<button type="submit">Add master</button>
		</form>
	</div>

	<sql:setDataSource var="dataBase" driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/salon" user="root"
		password="4815162342" />

	<sql:query dataSource="${dataBase}" var="masters">
		SELECT * FROM masters
	</sql:query>

	<div>
		<table border="1">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Second name</th>
				<th>Email</th>
			</tr>

			<c:forEach var="master" items="${masters.rows}">
				<tr>
					<td><c:out value="${master.id}" /></td>
					<td><c:out value="${master.name}" /></td>
					<td><c:out value="${master.secondName}" /></td>
					<td><c:out value="${master.email}" /></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>