<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Salon</title>
</head>

<body>
	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<div>
		<div>
			<a href="showmasters.jsp"><button type="submit">Manage
					masters</button></a>
		</div>

		<div>
			<a href="showservices.jsp"><button type="submit">Manage
					services</button></a>
		</div>

		<div>
			<a href="showorders.jsp"><button type="submit">Manage
					orders</button></a>
		</div>
	</div>
</body>
</html>