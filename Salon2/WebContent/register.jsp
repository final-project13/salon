<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
</head>

<body>
	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<form action="Register" method="post">
		<div>
			<label for="name">Name</label> <input type="text" name="name"
				id="name" placeholder="Name" />
		</div>
		<div>
			<label for="secondname">Second name</label> <input type="text"
				name="secondname" id="secondname" placeholder="Second name" />
		</div>
		<div>
			<label for="email">Email</label> <input type="email" name="email"
				id="email" placeholder="Email" />
		</div>
		<div>
			<label for="password">Password</label> <input type="password"
				name="password" id="password" placeholder="Password" />
		</div>
		<div>
			<label for="confpass">Confirm password</label> <input type="password"
				name="confpass" id="confpass" placeholder="Password" />
		</div>
		<button type="submit">Register</button>
	</form>
</body>
</html>