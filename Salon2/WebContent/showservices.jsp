<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Salon</title>
</head>

<body>
	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<div>
		<form action="AddService" method="post">
			<div>
				<label for="name">Service</label> <input type="text" name="name"
					id="name" placeholder="Service" />
			</div>

			<button type="submit">Add service</button>
		</form>
	</div>

	<sql:setDataSource var="dataBase" driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/salon" user="root"
		password="4815162342" />

	<sql:query dataSource="${dataBase}" var="services">
		SELECT * FROM services
	</sql:query>

	<div>
		<table border="1">
			<tr>
				<th>ID</th>
				<th>Name</th>
			</tr>

			<c:forEach var="service" items="${services.rows}">
				<tr>
					<td><c:out value="${service.id}" /></td>
					<td><c:out value="${service.name}" /></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>