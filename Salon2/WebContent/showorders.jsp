<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Salon</title>
</head>

<body>
	<sql:setDataSource var="dataBase" driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/salon" user="root"
		password="4815162342" />

	<sql:query dataSource="${dataBase}" var="orders">
		SELECT * FROM orders
	</sql:query>

	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<form action="OrderManager" method="post">
		<div>
			<table border="1">
				<tr>
					<th>Order ID</th>
					<th>Client ID</th>
					<th>Master ID</th>
					<th>Service</th>
					<th>Time</th>
					<th>Delete ID</th>
				</tr>

				<c:forEach var="order" items="${orders.rows}">
					<tr>
						<td><c:out value="${order.orderID}" /></td>
						<td><c:out value="${order.clientID}" /></td>
						<td><c:out value="${order.masterID}" /></td>
						<td><c:out value="${order.service}" /></td>
						<td><c:out value="${order.time}" /></td>
						<td><input type="submit" value="${order.orderID}"
							name="orderID" /></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</form>
</body>
</html>