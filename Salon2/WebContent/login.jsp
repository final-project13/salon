<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>

<body>
	<div>
		<p>
			<a href="index.jsp">Home</a>
		</p>
	</div>

	<form action="Login" method="post">
		<div>
			<label for="email">Email</label> <input type="email" name="email"
				id="email" placeholder="Email" />
		</div>
		<div>
			<label for="password">Password</label> <input type="password"
				name="password" id="password" placeholder="Password" />
		</div>
		<button type="submit">Login</button>
	</form>
</body>
</html>