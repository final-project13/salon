<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Salon</title>
</head>

<body>
	<div>
		<p>
			<a href="login.jsp">Login</a> / <a href="register.jsp">Register</a>
		</p>
	</div>

	<sql:setDataSource var="dataBase" driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/salon" user="root"
		password="4815162342" />

	<sql:query dataSource="${dataBase}" var="services">
		SELECT name FROM services
	</sql:query>

	<div>
		<table border="1">
			<tr>
				<th>Services</th>
			</tr>

			<c:forEach var="service" items="${services.rows}">
				<tr>
					<td><c:out value="${service.name}" /></td>
				</tr>
			</c:forEach>
		</table>
	</div>

	<sql:query dataSource="${dataBase}" var="masters">
		SELECT * FROM masters
	</sql:query>

	<div>
		<table border="1">
			<tr>
				<th colspan="3">Masters</th>
			</tr>

			<tr>
				<th>Name</th>
				<th>Second name</th>
				<th>Rate</th>
			</tr>

			<c:forEach var="master" items="${masters.rows}">
				<sql:query dataSource="${dataBase}" var="ratings">
					SELECT AVG(rate) AS rate
					FROM masters_ratings
					WHERE masterID = (
					SELECT id FROM masters
					WHERE email = '<c:out value="${master.email}" />')
				</sql:query>

				<tr>
					<td><c:out value="${master.name}" /></td>
					<td><c:out value="${master.secondName}" /></td>
					<td><c:forEach var="rating" items="${ratings.rows}">
							<c:choose>
								<c:when test="${rating.rate != null}">
									<c:out value="${rating.rate}" />
								</c:when>
								<c:otherwise>
									<c:out value="not rated" />
								</c:otherwise>
							</c:choose>
						</c:forEach></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>