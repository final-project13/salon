package main.beans;

public class ServiceBean {
	String name;
	
	/**
	 * @param name
	 */
	public ServiceBean(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}