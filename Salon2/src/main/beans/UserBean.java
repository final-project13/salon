package main.beans;

public class UserBean {
	private String name;
	private String secondName;
	private String email;
	private String password;
	private String confPass;

	/**
	 * @param email
	 * @param password
	 */
	public UserBean(String email, String password) {
		this.email = email;
		this.password = password;
	}

	/**
	 * @param name
	 * @param secondName
	 * @param email
	 * @param password
	 * @param confPass
	 */
	public UserBean(String name, String secondName, String email, String password, String confPass) {
		this.name = name;
		this.secondName = secondName;
		this.email = email;
		this.password = password;
		this.confPass = confPass;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public String getSecondName() {
		return secondName;
	}

	/**
	 * @param secondName
	 */
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return
	 */
	public String getConfPass() {
		return confPass;
	}

	/**
	 * @param confPass
	 */
	public void setConfPass(String confPass) {
		this.confPass = confPass;
	}
	
}