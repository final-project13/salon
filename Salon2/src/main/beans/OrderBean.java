package main.beans;

public class OrderBean {
	String client;
	String master;
	String service;
	String timeslot;
	
	/**
	 * @param client
	 * @param master
	 * @param service
	 * @param timeslot
	 */
	public OrderBean(String client, String master, String service, String timeslot) {
		this.client = client;
		this.master = master;
		this.service = service;
		this.timeslot = timeslot;
	}
	
	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}
	
	/**
	 * @param client
	 */
	public void setClient(String client) {
		this.client = client;
	}

	/**
	 * @return
	 */
	public String getMaster() {
		return master;
	}
	
	/**
	 * @param master
	 */
	public void setMaster(String master) {
		this.master = master;
	}
	
	/**
	 * @return
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param service
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * @return
	 */
	public String getTimeslot() {
		return timeslot;
	}

	/**
	 * @param timeslot
	 */
	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}
	
}