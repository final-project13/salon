package main.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import main.database.OrderDao;
import main.beans.OrderBean;

@WebServlet("/AddOrder")
public class AddOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddOrder() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String client = session.getAttribute("id").toString();
		String master = request.getParameter("master");
		String service = request.getParameter("service");
		String timeslot = request.getParameter("timeslot");
		
		OrderBean bean = new OrderBean(client, master, service, timeslot);
		OrderDao dao = new OrderDao();
		
		if(dao.addOrder(bean)) {
			response.getWriter().append("order accepted");
		}
		else {
			response.getWriter().append("order regected");
		}

	}

}