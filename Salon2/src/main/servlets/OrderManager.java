package main.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.database.OrderDao;

@WebServlet("/OrderManager")
public class OrderManager extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public OrderManager() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		OrderDao dao = new OrderDao();
		
		if (dao.cancelOrder(request.getParameter("orderID"))) {
			response.sendRedirect("showorders.jsp");
		}
		else {
			response.getWriter().append(request.getParameter("orderID"));
		}

	}

}