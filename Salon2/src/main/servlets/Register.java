package main.servlets;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.database.ClientDao;
import main.beans.UserBean;

@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Register() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] passBytes = md.digest(request.getParameter("password").getBytes());
			byte[] confBytes = md.digest(request.getParameter("confpass").getBytes());

			String name = request.getParameter("name");
			String secondName = request.getParameter("secondname");
			String email = request.getParameter("email");
			String password = new BigInteger(1, passBytes).toString(16);
			String confPass = new BigInteger(1, confBytes).toString(16);

			UserBean bean = new UserBean(name, secondName, email, password, confPass);
			ClientDao dao = new ClientDao();
			
			if(dao.addClient(bean)) {
				response.getWriter().append("register successful");
			}
			
		} catch (Exception ex) {
			System.out.println(ex);
		}

	}

}