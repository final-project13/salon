package main.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.beans.ServiceBean;
import main.database.ServiceDao;

@WebServlet("/AddService")
public class AddService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddService() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("name");
		
		ServiceBean bean = new ServiceBean(name);
		ServiceDao dao = new ServiceDao();
		
		if(dao.addService(bean)) {
			response.sendRedirect("showservices.jsp");
		}
		
	}

}