package main.servlets;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.beans.UserBean;
import main.database.MasterDao;

@WebServlet("/AddMaster")
public class AddMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddMaster() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] passBytes = md.digest(request.getParameter("password").getBytes());
			byte[] confBytes = md.digest(request.getParameter("confpass").getBytes());

			String name = request.getParameter("name");
			String secondName = request.getParameter("secondname");
			String email = request.getParameter("email");
			String password = new BigInteger(1, passBytes).toString(16);
			String confPass = new BigInteger(1, confBytes).toString(16);

			UserBean bean = new UserBean(name, secondName, email, password, confPass);
			MasterDao dao = new MasterDao();
			
			if(dao.addMaster(bean)) {
				
			}
			
		} catch (Exception ex) {
			System.out.println(ex);
		}

	}

}
