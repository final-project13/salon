package main.servlets;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import main.beans.UserBean;
import main.database.UserDao;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] passBytes = md.digest(request.getParameter("password").getBytes());

			String email = request.getParameter("email");
			String password = new BigInteger(1, passBytes).toString(16);

			UserBean bean = new UserBean(email, password);
			UserDao dao = new UserDao();
			
			HttpSession session = request.getSession();

			switch (dao.getUserRole(bean)) {
			case "client": {
				response.sendRedirect("client.jsp");
				session.setAttribute("id", dao.getUserID(bean));
				break;
			}
			case "master": {
				response.sendRedirect("master.jsp");
				session.setAttribute("id", dao.getUserID(bean));
				break;
			}
			case "admin": {
				response.sendRedirect("admin.jsp");
				break;
			}
			default: {
				response.getWriter().append("no such user");
			}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}