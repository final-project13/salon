package main.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class DB {
	private static Connection conn;
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/salon";
	private static final String USER = "root";
	private static final String PASS = "4815162342";
	
	/**
	 * @return
	 */
	public static Connection getConn() {

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASS);
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return conn;
	}
}