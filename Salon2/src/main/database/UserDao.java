package main.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.beans.UserBean;

public class UserDao {
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	public UserDao() {
		conn = DB.getConn();

		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param bean
	 * @return
	 */
	public String getUserRole(UserBean bean) {
		String email = bean.getEmail();
		String password = bean.getPassword();

		try {
			rs = stmt.executeQuery("SELECT email, password FROM clients");
			while (rs.next()) {
				if (email.equals(rs.getString("email")) && password.equals(rs.getString("password"))) {
					return "client";
				}
			}
			
			rs = stmt.executeQuery("SELECT email, password FROM masters");
			while (rs.next()) {
				if (email.equals(rs.getString("email")) && password.equals(rs.getString("password"))) {
					return "master";
				}
			}
			
			rs = stmt.executeQuery("SELECT email, password FROM admins");
			while (rs.next()) {
				if (email.equals(rs.getString("email")) && password.equals(rs.getString("password"))) {
					return "admin";
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return "no such user";

	}
	
	/**
	 * @param bean
	 * @return
	 */
	public String getUserID(UserBean bean) {
		String email = bean.getEmail();
		String password = bean.getPassword();

		try {
			rs = stmt.executeQuery("SELECT id, email, password FROM clients");
			while (rs.next()) {
				if (email.equals(rs.getString("email")) && password.equals(rs.getString("password"))) {
					return rs.getString("id");
				}
			}
			
			rs = stmt.executeQuery("SELECT id, email, password FROM masters");
			while (rs.next()) {
				if (email.equals(rs.getString("email")) && password.equals(rs.getString("password"))) {
					return rs.getString("id");
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
		
		return "";
	}
}