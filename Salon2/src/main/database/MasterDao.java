package main.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.beans.UserBean;

public class MasterDao {
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	public MasterDao() {
		conn = DB.getConn();

		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param bean
	 * @return
	 */
	public boolean addMaster(UserBean bean) {
		String name = bean.getName();
		String secondName = bean.getSecondName();
		String email = bean.getEmail();
		String password = bean.getPassword();
		String confPass = bean.getConfPass();

		if (password.equals(confPass)) {
			try {
				rs = stmt.executeQuery("SELECT email, password FROM masters");
				while (rs.next()) {
					if (email.equals(rs.getString("email")) || password.equals(rs.getString("password"))) {
						return false;
					}
				}

				stmt.executeUpdate(String.format(
						"INSERT INTO masters (name, secondName, email, password) VALUES ('%1$s', '%2$s', '%3$s', '%4$s')",
						name, secondName, email, password));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			return false;
		}

		return true;
	}
}