package main.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import main.beans.OrderBean;

public class OrderDao {
	Connection conn = null;
	Statement stmt = null;

	public OrderDao() {
		conn = DB.getConn();

		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param bean
	 * @return
	 */
	public boolean addOrder(OrderBean bean) {
		String client = bean.getClient();
		String[] master = bean.getMaster().split(" ");
		String service = bean.getService();
		String time = bean.getTimeslot();

		try {
			stmt.executeUpdate(String.format(
					"INSERT INTO orders (clientID, masterID, service, time) VALUES ('%1$s', (SELECT id FROM masters WHERE name = '%2$s' AND secondName = '%3$s'), '%4$s', '2021-01-01 %5$s:00')",
					client, master[0], master[1], service, time));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	/**
	 * @param orderID
	 * @return
	 */
	public boolean cancelOrder(String orderID) {
		try {
			stmt.executeUpdate(String.format("DELETE FROM orders WHERE orderID = '%s'", orderID));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}