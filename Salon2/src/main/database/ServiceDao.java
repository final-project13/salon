package main.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.beans.ServiceBean;

public class ServiceDao {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	public ServiceDao() {
		conn = DB.getConn();

		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param bean
	 * @return
	 */
	public boolean addService(ServiceBean bean) {
		String name = bean.getName();

		try {
			stmt.executeUpdate(String.format("INSERT INTO services (name) VALUES ('%s')", name));
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}