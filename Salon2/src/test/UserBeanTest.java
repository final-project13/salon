package test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.UserBean;

public class UserBeanTest {
	
	private static UserBean bean;

	@BeforeClass
	public static void beforeTest() {
		bean = new UserBean(null, null);

	}

	@Test
	public void userBeanTest() {
		bean.setConfPass("test conf pass");
		assertEquals("test conf pass", bean.getConfPass());
		bean.setEmail("test email");
		assertEquals("test email", bean.getEmail());
		bean.setName("test name");
		assertEquals("test name", bean.getName());
		bean.setPassword("test pass");
		assertEquals("test pass", bean.getPassword());
		bean.setSecondName("test second name");
		assertEquals("test second name", bean.getSecondName());
	}

}