package test;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.UserBean;
import main.database.ClientDao;
import main.database.MasterDao;
import main.database.UserDao;

public class UserDaoTest {

	private static Connection conn;
	private static Statement stmt;
	private static ResultSet rs;

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/salon";
	private static final String USER = "root";
	private static final String PASS = "4815162342";

	private UserBean userBean;

	private UserDao userDao;
	private ClientDao clientDao;
	private MasterDao masterDao;

	@BeforeClass
	public static void setConnection() {
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASS);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Before
	public void beforeTest() {
		userBean = new UserBean("test name", "test secondName", "test email", "test pass", "test pass");
		userDao = new UserDao();
		masterDao = new MasterDao();
		clientDao = new ClientDao();
	}

	@Test
	public void getUserRoleTest() {
		try {
			stmt.executeUpdate(
					"INSERT INTO admins (name, secondName, email, password) VALUES ('test name', 'test secondName', 'test email', 'test pass')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertEquals("admin", userDao.getUserRole(userBean));

		masterDao.addMaster(userBean);
		assertEquals("master", userDao.getUserRole(userBean));

		clientDao.addClient(userBean);
		assertEquals("client", userDao.getUserRole(userBean));
	}

	@Test
	public void getUserIDTest() {
		try {
			masterDao.addMaster(userBean);
			rs = stmt.executeQuery("SELECT id FROM masters WHERE name = 'test name'");
			rs.last();
			assertEquals(rs.getString("id"), userDao.getUserID(userBean));

			clientDao.addClient(userBean);
			rs = stmt.executeQuery("SELECT id FROM clients WHERE name = 'test name'");
			rs.last();
			assertEquals(rs.getString("id"), userDao.getUserID(userBean));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@After
	public void clear() {
		try {
			stmt.executeUpdate("DELETE FROM masters WHERE name = 'test name'");
			stmt.executeUpdate("DELETE FROM clients WHERE name = 'test name'");
			stmt.executeUpdate("DELETE FROM admins WHERE name = 'test name'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}