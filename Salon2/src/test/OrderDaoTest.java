package test;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.UserBean;
import main.beans.OrderBean;
import main.beans.ServiceBean;
import main.database.ClientDao;
import main.database.MasterDao;
import main.database.OrderDao;
import main.database.ServiceDao;

public class OrderDaoTest {

	private static Connection conn;
	private static Statement stmt;
	private static ResultSet rs;

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/salon";
	private static final String USER = "root";
	private static final String PASS = "4815162342";

	private UserBean userBean;
	private OrderBean orderBean;
	private ServiceBean serviceBean;

	private ClientDao clientDao;
	private MasterDao masterDao;
	private ServiceDao serviceDao;
	private OrderDao orderDao;

	private String client;
	private String master;
	private String service;
	private String orderID;

	@BeforeClass
	public static void setConnection() {
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASS);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Before
	public void beforeTest() {
		userBean = new UserBean("name", "secondName", "test email", "test pass", "test pass");
		serviceBean = new ServiceBean("test service");
		masterDao = new MasterDao();
		clientDao = new ClientDao();
		serviceDao = new ServiceDao();

		masterDao.addMaster(userBean);
		clientDao.addClient(userBean);
		serviceDao.addService(serviceBean);

		try {
			rs = stmt.executeQuery("SELECT id FROM clients");
			rs.last();
			client = rs.getString("id");
			master = "name secondName";
			service = "test service";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		orderBean = new OrderBean(client, master, service, "13:13");
		orderDao = new OrderDao();
	}

	@Test
	public void addOrderTest() {
		orderDao.addOrder(orderBean);
		try {
			rs = stmt.executeQuery("SELECT id FROM masters");
			rs.last();
			master = rs.getString("id");

			rs = stmt.executeQuery("SELECT * FROM orders");
			rs.last();

			assertEquals(client, rs.getString("clientID"));
			assertEquals(master, rs.getString("masterID"));
			assertEquals(service, rs.getString("service"));

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void cancelOrder() {
		orderDao.addOrder(orderBean);
		try {
			rs = stmt.executeQuery("SELECT orderID FROM orders WHERE service = 'test service'");
			rs.last();
			orderID = rs.getString("orderID");
			orderDao.cancelOrder(orderID);
			
			rs = stmt.executeQuery("SELECT orderID FROM orders WHERE service = 'test service'");

			assertEquals(true, !rs.next());

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@After
	public void clear() {
		try {
			stmt.executeUpdate("DELETE FROM masters WHERE name = 'name'");
			stmt.executeUpdate("DELETE FROM clients WHERE name = 'name'");
			stmt.executeUpdate("DELETE FROM services WHERE name = 'test service'");
			stmt.executeUpdate("DELETE FROM orders WHERE service = 'test service'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}