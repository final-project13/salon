package test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.OrderBean;

public class OrderBeanTest {
	
	private static OrderBean bean;
	
	@BeforeClass
	public static void beforeTest() {
		bean = new OrderBean(null, null, null, null);
		
	}
	
	@Test
	public void orderBeanTest() {
		bean.setClient("test client");
		assertEquals("test client", bean.getClient());
		bean.setMaster("test master");
		assertEquals("test master", bean.getMaster());
		bean.setService("test service");
		assertEquals("test service", bean.getService());
		bean.setTimeslot("test timeslot");
		assertEquals("test timeslot", bean.getTimeslot());
	}
	
}