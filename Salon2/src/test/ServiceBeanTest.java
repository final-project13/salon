package test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.ServiceBean;

public class ServiceBeanTest {

	private static ServiceBean bean;

	@BeforeClass
	public static void beforeTest() {
		bean = new ServiceBean(null);

	}

	@Test
	public void serviceBeanTest() {
		bean.setName("test name");
		assertEquals("test name", bean.getName());
	}

}