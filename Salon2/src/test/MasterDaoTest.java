package test;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.UserBean;
import main.database.MasterDao;

public class MasterDaoTest {
	
	private static Connection conn;
	private static Statement stmt;
	private static ResultSet rs;
	
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/salon";
	private static final String USER = "root";
	private static final String PASS = "4815162342";

	private UserBean bean;
	private MasterDao dao;
	
	@BeforeClass
	public static void setConnection() {
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASS);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	@Before
	public void beforeTest() {
		bean = new UserBean("test name", "test second name", "test email", "test pass", "test pass");
		dao = new MasterDao();
	}

	@Test
	public void masterDaoTest() {
		dao.addMaster(bean);
		
		try {
			rs = stmt.executeQuery("SELECT * FROM masters");
			rs.last();
			
			assertEquals("test name", rs.getString("name"));
			assertEquals("test second name", rs.getString("secondName"));
			assertEquals("test email", rs.getString("email"));
			assertEquals("test pass", rs.getString("password"));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@After
	public void clear() {
		try {
			stmt.executeUpdate("DELETE FROM masters WHERE name = 'test name'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}