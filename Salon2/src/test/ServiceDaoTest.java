package test;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.beans.ServiceBean;
import main.database.ServiceDao;

public class ServiceDaoTest {
	
	private static Connection conn;
	private static Statement stmt;
	private static ResultSet rs;

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/salon";
	private static final String USER = "root";
	private static final String PASS = "4815162342";
	
	private ServiceBean serviceBean;
	private ServiceDao serviceDao;
	
	@BeforeClass
	public static void setConnection() {
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASS);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void beforeTest() {
		serviceBean = new ServiceBean("test service");
		serviceDao = new ServiceDao();
	}
	
	@Test
	public void serviceDaoTest() {
		serviceDao.addService(serviceBean);
		
		try {
			rs = stmt.executeQuery("SELECT name FROM services");
			rs.last();

			assertEquals("test service", rs.getString("name"));

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void clear() {
		try {
			stmt.executeUpdate("DELETE FROM services WHERE name = 'test service'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}